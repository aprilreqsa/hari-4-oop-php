<?php
include_once ('animal.php');
include_once ('ape.php');
include_once ('frog.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>";
echo "Leg : " . $sheep->legs; // 4
echo "<br>";
echo "cold blooded : " . $sheep->cold_blooded; // "no"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name; // "shaun"
echo "<br>";
echo "Leg : " . $kodok->legs; // 4
echo "<br>";
echo "cold blooded : " . $kodok->cold_blooded; // "no"
echo "<br>";
echo "Jump : " ;
$kodok->jump() ; // "hop hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name; // "shaun"
echo "<br>";
echo "Leg : " . $sungokong->legs; // 4
echo "<br>";
echo "cold blooded : " . $sungokong->cold_blooded; // "no"
echo "<br>";
echo "Yell : " ;
$sungokong->yell(); // "Auooo"
echo "<br><br>";

